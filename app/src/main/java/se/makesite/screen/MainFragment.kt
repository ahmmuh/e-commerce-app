package se.makesite.screen

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import kotlinx.android.synthetic.main.main_fragment.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import se.makesite.data.Products
import se.makesite.ProductAdapter
import se.makesite.ecommerce.R
import java.net.URL

class MainFragment : Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.main_fragment, container, false)
   doAsync {
       val json = URL("https://finepointmobile.com/data/products.json").readText()
       uiThread {
           d("Ahmed", "json $json")
           val products = Gson().fromJson(json, Array<Products>::class.java).toList()
           root.recyclerView.apply {
               layoutManager = GridLayoutManager(activity,2)
               adapter = ProductAdapter(products)
               root.progressBar.visibility = View.GONE
       }
   }

   }
      return root
    }
}