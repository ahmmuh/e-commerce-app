package se.makesite

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import se.makesite.screen.ProductsDetails
import se.makesite.data.Products
import se.makesite.ecommerce.R

class ProductAdapter(private val products: List<Products>) :

    RecyclerView.Adapter<ProductAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.product_row,
        parent,false)
      val holder = ViewHolder(view)
        view.setOnClickListener {
        val intent = Intent(parent.context, ProductsDetails::class.java)
            intent.putExtra("title",products[holder.adapterPosition].title)
            intent.putExtra("photoUrl",products[holder.adapterPosition].photosUrl)

            parent.context.startActivity(intent)

        }
        return holder



    }

    override fun getItemCount() = products.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // picasso är plugin to  implement image url into your app
        Picasso.get().load(products[position].photosUrl).into(holder.image)
        val product = products[position]
        holder.title.text = product.title
        holder.price.text = product.price.toString()


    }


    class ViewHolder (itemView: View)  : RecyclerView.ViewHolder(itemView){

        val image : ImageView = itemView.findViewById(R.id.photo)
        val title : TextView = itemView.findViewById(R.id.title)
        val price : TextView = itemView.findViewById(R.id.price)





    }


}
